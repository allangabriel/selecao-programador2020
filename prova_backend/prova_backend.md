# Prova de conhecimentos de backend (Python)

1. O que é o PEP8?

```
Um documento com convenções de organização e formatação de codigo.
```

2. Nomeie alguns dos principais frameworks para o Python de cada área que você conhece.

```
Flask e Django
```

3.  Qual (ou quais) das seguintes bibliotecas não vem por padrão na instação do Python? (Battery included)

```
numpy e pandas
```

```python
import re
import cmath
import numpy
import functools
import sqlite
import pandas
```

4. Explique o que o seguinte código faz.

```
Adiciona 'quem' no placeholder de uma string, salva em uma variavel e imprime ao contrario a partir do ultimo caractere.

```

```python
oi = '''Este é um "texto" para '{}'''.format('quem')
print(oi[::-1])
```

5. Conserte o funcionamento do seguinte trecho

```python
import functools

x = [1, 2, 3, 4, 5, 6, 7, 8]

soma_pares = functools.reduce(lambda x, y: x+y, (y for y in x if not y % 2))
```

6. No Django o que são Models?
```
Estruturas dos dados utilizados no sistema.
```

7. O que é um ORM?

```
Object-Relational-Mapping serve para interagir com bancos de dados relacionais.
```

8. Quais as diferenças e vantagens entre as "Function Based Views" e as "Class Based Views"?

9. Explique o que é o padrão MVC.

```
É um padrão de arquitetura de software geralmente utlizado em aplicações web.

Models: Contem as estruturas de dados que o sistema utiliza.
View: Interfaces de usuario/Meios de Interagir com o sistema.
Controller: Gerencia as duas anteriores.
```

10. Com o projeto que está nesta pasta, crie uma API que permita cadastrar livros, e outra para ver a lista de livros.

```
Não tive tempo para estudar django. Então decidi fazer a proposta desse desafio utilizando node.js,mongodb, express e mongoose. Está no diretorio 'projeto-node' e no readme tem instruções de comoexecutar. Espero que a proposta seja satisfatoria.
```
    > Os campos necessários para a tabela serão: id, titulo (string), autor (string)
    > Dica: Os tutoriais 1 (https://www.django-rest-framework.org/tutorial/1-serialization/) ao 3 mostram o funcionamento
    > básico de como operar com a biblioteca.
