const mongoose = require("mongoose");

const Livros = new mongoose.Schema({
  titulo: String,
  autor: String,
});

module.exports = mongoose.model("livro", Livros);
