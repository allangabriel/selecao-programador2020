require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const Livro = require("./models/livros");

const app = express();

mongoose
  .connect(process.env.DBURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(console.log("DB Connected."))
  .catch((err) => console.error(err));

app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", async (req, res) => {
  res.redirect("/livros");
});

app.get("/livros", async (req, res) => {
  const livros = await Livro.find({});

  res
    .status(livros.length ? 200 : 404)
    .json({ statusCode: res.statusCode, livros });
});

app.post("/livros", async (req, res) => {
  const { titulo, autor } = req.body;

  await Livro.create({
    titulo,
    autor,
  })
    .then(res.status(201).send({ message: "Livro Registrado." }))
    .catch(({ message }) => res.status(500).send({ message }));
});

app.listen(process.env.PORT, () =>
  console.log(`Listening at ${process.env.PORT}`)
);
