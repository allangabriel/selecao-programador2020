# Prova de conhecimentos de frontend

1. O que é ECMAScript?

```
Uma convenção de padronização do javascript.
```

2. Como podemos usar Javascript no backend?

```
A partir de algum runtime engine como o node.js ou deno.
```

3. Nomeie os principais frameworks javascript e sua principal utilidade.

```
React: SPA
Vue.JS: SPA
Angular: SPA
Express.js: Web Apps
```

4. Quais linguagens, frameworks e bibliotecas você identifica do seguinte código?

```
Javascript e jQuery
```

```javascript
$(document).ready({
  var node = $('#nome')
  node.change({
    console.log(nome.val())
  })
})
```

5. Explique o que o seguinte código faz:

```
  Seleciona todos os elementos input do document e converte a node list que o metodo getElementsByTagName() retorna em um array e salva esse array em uma constante. Após isso, ele altera o atributo value de todos os elementos para "Hello World".
```

```javascript
const nodes = Array.from(document.getElementsByTagName("input"));
nodes.map((node) => {
  node.value = "Hello World";
});
```

6. Conserte o seguinte código:

```javascript
const lista = [
  { nome: "item 1", valor: 1 },
  { nome: "item 2", valor: 2 },
  { nome: "item 3", valor: 3 },
  { nome: "item 4", valor: 4 },
  { nome: "item 5", valor: 5 },
  { nome: "item 6", valor: 6 },
];

let listaVisivel = lista;

function pares() {
  listaVisivel = lista.filter((val) => !(val.valor % 2));
}

function impares() {
  listaVisivel = lista.filter((val) => val.valor % 2);
}
```

7. Utilizando o projeto no diretório `projeto` implemente um componente SELECT que ao inicializar consulte a API
   `https://jsonplaceholder.typicode.com/users` e monte o combo com o ID do usuário como `value` e exibindo o nome
   do usuário no combo.

8. Continuando de onde a questão 7 parou, crie um botão `Ver posts` que ao ser clicado irá exibir uma tabela abaixo
   do combo com os posts publicados por aquele usuário.
   > A API dos posts pode ser acessada através da URL https://jsonplaceholder.typicode.com/posts?userId=X onde no lugar do
   > X você irá colocar o id do usuário selecionado no combo. Ex: https://jsonplaceholder.typicode.com/posts?userId=1
