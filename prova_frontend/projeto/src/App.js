import Select from "./components/select/select.component";

import "./App.css";

function App() {
  return (
    <div className="App">
      <Select />
    </div>
  );
}

export default App;
