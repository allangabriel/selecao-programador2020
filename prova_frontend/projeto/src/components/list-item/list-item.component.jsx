import React from "react";

import "./list-item.styles.css";

const ListItem = ({ content }) => <li className="list-item">{content}</li>;

export default ListItem;
