import React from "react";

import "./list.styles.css";

const List = ({ children, show }) => (
  <ul className="list" style={{ display: show ? "block" : "none" }}>
    {children}
  </ul>
);

export default List;
