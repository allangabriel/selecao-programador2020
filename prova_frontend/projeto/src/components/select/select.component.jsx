import React from "react";
import Card from "../card/card.component";

import "./select.styles.css";

class Select extends React.Component {
  constructor() {
    super();
    this.state = { users: [] };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((data) => this.setState({ users: data }));
  }

  render() {
    const { users } = this.state;

    return (
      <div className="select">
        {users.map((user) => (
          <Card key={user.id} id={user.id} name={user.name} />
        ))}
      </div>
    );
  }
}

export default Select;
