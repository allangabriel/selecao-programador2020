import React from "react";
import Button from "../button/button.component";
import List from "../list/list.component";
import ListItem from "../list-item/list-item.component";

import "./card.styles.css";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      showList: false,
    };
  }

  handleListHide = () => {
    this.setState(({ showList }) => ({ showList: !showList }));
  };

  componentDidMount() {
    fetch(`https://jsonplaceholder.typicode.com/posts?userId=${this.props.id}`)
      .then((res) => res.json())
      .then((data) => this.setState({ posts: data }));
  }

  render() {
    const { posts, showList } = this.state;
    const { id, name } = this.props;

    return (
      <div className="card">
        <div className="card-header">
          <h2>{`${id} - ${name}`}</h2>
        </div>
        <Button
          placeholder={showList ? "Recolher Lista" : "Ver Posts"}
          color="#F45D01"
          handleClick={this.handleListHide}
        />
        <List show={showList}>
          {posts.map(({ id, title }) => (
            <ListItem key={id} content={title} />
          ))}
        </List>
      </div>
    );
  }
}

export default Card;
