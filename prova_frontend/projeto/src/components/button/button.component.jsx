import React from "react";

import "./button.styles.css";

const Button = ({ handleClick, placeholder, color }) => (
  <button
    className="button"
    style={{ backgroundColor: color }}
    onClick={handleClick}
  >
    {placeholder}
  </button>
);

export default Button;
