# Prova de conhecimentos gerais

1.  Explique os seguintes conceitos: Variáveis, Loops, Funções?

    ```
    Variáveis: São uma forma de armazenar dados na memoria que podem ser alterados e utilizados.

    Loops: São estruturas de repetição, e como tal repetem uma tarefa até que uma determinada condição se torne inválida.

    Funções: São trechos de codigo que desempenham uma tarefa podendo ter parâmetros e retornar algo ou não.
    ```

2.  O que significa ser "Case Sensitive"?

    ```
    Significa que há diferenciação entre caracteres minúsculos e maiúsculos.
    ```

3.  O que é um sistema de versionamento? Cite pelo menos 2.

    ```
    Um software que tem como objetivo manter os registros feitos pelos envolvidos no projeto. GIT e CVS.
    ```

4.  No GIT, o que seria um COMMIT?

    ```
    O registro de uma alteração.
    ```

5.  No GIT, o que seria um MERGE?

    ```
    Sobrescrever um branch com commits de outro branch.
    ```

6.  Qual a importância de se utilizar um sistema de versionamento?

    ```
    Manter registro do progresso e das alterações feitas no projeto, quem as fez e versões do projeto.
    ```

7.  Utilizando o banco de dados fornecido no arquivo `chinook.db` prepare uma consulta que mostre o nome do artista e a quantidade albuns que ele lançou.

    ```sql
    SELECT artists.Name,
           COUNT(albums.AlbumId) AS "Albuns Lancados"
    FROM artists
    JOIN albums ON artists.ArtistId = albums.ArtistId
    GROUP BY artists.ArtistId;
    ```

8.  Prepare uma consulta que traga os 5 artistas que lançaram mais alguns e quantos albuns estes lançaram.

    ```sql
    SELECT artists.Name,
           COUNT(albums.AlbumId) AS "Albuns Lancados"
    FROM artists
    JOIN albums ON artists.ArtistId = albums.ArtistId
    GROUP BY artists.ArtistId
    ORDER BY 2 DESC
    LIMIT 5;
    ```

9.  Prepare uma consulta que traga os 20 clientes que mais gastaram. (Na tabela `invoices` há as compras feitas por cada cliente e seu valor)

    ```sql
    SELECT customers.FirstName || " " || customers.LastName AS "Nome do Cliente",
           SUM(invoices.Total) AS "Total Gasto"
    FROM customers
    JOIN invoices ON customers.CustomerId = invoices.CustomerId
    GROUP BY customers.CustomerId
    ORDER BY 2 DESC
    LIMIT 20;
    ```

10. Prepare uma consulta que traga os seguintes dados: `CustomerId | CustomerName | total2011 | total2012 | total2013` Onde as colunas de totais mostram quanto aquele cliente gastou naquele determinado ano.
    ```sql
    SELECT customers.CustomerId,
           customers.FirstName || " " || customers.LastName AS "Nome do Cliente",
           SUM(CASE
                   WHEN strftime("%Y", invoices.InvoiceDate) = "2011" THEN invoices.Total
                   ELSE 0
               END) AS "Total 2011",
           SUM(CASE
                   WHEN strftime("%Y", invoices.InvoiceDate) = "2012" THEN invoices.Total
                   ELSE 0
               END) AS "Total 2012",
           SUM(CASE
                   WHEN strftime("%Y", invoices.InvoiceDate) = "2013" THEN invoices.Total
                   ELSE 0
               END) AS "Total 2013"
    FROM customers
    JOIN invoices ON customers.CustomerId = invoices.CustomerId
    GROUP BY customers.CustomerId;
    ```
